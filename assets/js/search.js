const btnSearch = document.getElementById("btnSearch");
const searchValue = document.getElementById("searchTitle");

function getData() {
    return JSON.parse(localStorage.getItem(STORAGE_KEY)) || books;
}

btnSearch.addEventListener("click", function (event) {
    event.preventDefault();
    const getByTitle = getData().filter(a => a.title == searchValue.value.trim());
    if (getByTitle.length == 0) {
        const getByAuthor = getData().filter(a => a.author == searchValue.value.trim());
        if (getByAuthor.length == 0) {
            const getByYear = getData().filter(a => a.year == searchValue.value.trim());
            if (getByYear.length == 0) {
                alert(`Data yang anda cari tidak ditemukan`);
                return location.reload();
            } else {
                showSearchResult(getByYear);
            }
        } else {
            showSearchResult(getByAuthor);
        }
    } else {
        showSearchResult(getByTitle);
    }

    searchValue.value = '';
})

function showSearchResult(books) {
    const searchResult = document.getElementById("search-item");

    searchResult.innerHTML = "";

    for (book of books) {
        const imageBook = document.createElement('img');
        imageBook.setAttribute("src", "assets/img/buku.png");

        const book_title = document.createElement("h3");
        book_title.innerText = book.title;

        const author_name = document.createElement("h4");
        author_name.innerText = book.author;

        const year_book = document.createElement("p");
        year_book.classList.add("year")
        year_book.innerText = book.year;

        const read_information = document.createElement("p");
        read_information.innerText = book.isCompleted ? 'Already Read' : 'Unread';

        const detail = document.createElement("div");
        detail.classList.add("detail_book");
        detail.append(book_title, author_name, year_book, read_information);

        const container = document.createElement('div');
        container.classList.add("search");
        container.append(imageBook, detail);

        searchResult.append(container);
    }
}