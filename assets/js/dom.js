const UNCOMPLETED_LIST_BOOK_ID = "unread";
const COMPLETED_LIST_BOOK_ID = "completed-read";
const BOOK_ITEMID = "itemId";
const SEARCH_LIST_BOOK = "search-item"

function addBook() {
    let targetBook;
    const title = document.getElementById("inputTitle").value;
    const author = document.getElementById("inputAuthor").value;
    const year = document.getElementById("inputYear").value;
    const isCompleted = document.getElementById('read').checked;

    const book = makeBook(title, author, year, isCompleted);
    const bookObject = composeBookObject(title, author, year, isCompleted);

    book[BOOK_ITEMID] = bookObject.id;
    books.push(bookObject);

    if (isCompleted) {
        targetBook = COMPLETED_LIST_BOOK_ID;
    } else {
        targetBook = UNCOMPLETED_LIST_BOOK_ID;
    }

    document.getElementById(targetBook).append(book);
    updateDataToStorage();
}

function makeBook(bookTitle, authorName, yearBook, isCompleted) {

    const imageBook = document.createElement('img');
    imageBook.setAttribute("src", "assets/img/buku.png");

    const book_title = document.createElement("h3");
    book_title.innerText = bookTitle;

    const author_name = document.createElement("h4");
    author_name.innerText = authorName;

    const year_book = document.createElement("p");
    year_book.innerText = yearBook;

    const detail = document.createElement("div");
    detail.classList.add("detail_book");
    detail.append(book_title, author_name, year_book);

    const container = document.createElement('div');
    container.classList.add("item", "shadow");
    container.append(imageBook, detail);

    if (isCompleted) {
        container.append(
            createUndoButton(),
            createTrashButton()
        );
    } else {
        container.append(
            createCheckButton(),
            createTrashButton()
        );
    }

    return container;
}

function createButton(buttonTypeClass, eventListener) {
    const button = document.createElement("button");
    button.classList.add(buttonTypeClass);
    button.addEventListener("click", function (event) {
        eventListener(event);
    });
    return button;
}

function addBookToCompleted(bookElement) {
    const listCompleted = document.getElementById(COMPLETED_LIST_BOOK_ID);
    const book_title = bookElement.querySelector(".detail_book > h3").innerText;
    const author_name = bookElement.querySelector(".detail_book > h4").innerText;
    const year_book = bookElement.querySelector(".detail_book > p").innerText;

    const newBook = makeBook(book_title, author_name, year_book, true);
    const book = findBook(bookElement[BOOK_ITEMID]);
    book.isCompleted = true;
    newBook[BOOK_ITEMID] = book.id;

    listCompleted.append(newBook);
    bookElement.remove();

    updateDataToStorage();
}

function undoBookFromCompleted(bookElement) {
    const listUncompleted = document.getElementById(UNCOMPLETED_LIST_BOOK_ID);
    const book_title = bookElement.querySelector(".detail_book > h3").innerText;
    const author_name = bookElement.querySelector(".detail_book > h4").innerText;
    const year_book = bookElement.querySelector(".detail_book > p").innerText;

    const newBook = makeBook(book_title, author_name, year_book, false);

    const book = findBook(bookElement[BOOK_ITEMID]);
    book.isCompleted = false;
    newBook[BOOK_ITEMID] = book.id;

    listUncompleted.append(newBook);
    bookElement.remove();

    updateDataToStorage();
}

function createCheckButton() {
    return createButton("check-button", function (event) {
        addBookToCompleted(event.target.parentElement);
    });
}

function createTrashButton() {
    return createButton("trash-button", function (event) {
        var deleteBook = confirm("are you sure to delete this book?");
        if (deleteBook) {
            removeBook(event.target.parentElement);
            alert("This book has been deleted");
        } else {
            alert("This book is not deleted");
        }
    });
}

function removeBook(bookElement) {
    const bookPosition = findBookIndex(bookElement[BOOK_ITEMID]);
    books.splice(bookPosition, 1);

    bookElement.remove();
    updateDataToStorage();
}

function createUndoButton() {
    return createButton("undo-button", function (event) {
        undoBookFromCompleted(event.target.parentElement);
    });
}

function booksLength() {
    const numberBook = document.getElementById('numberBook');
    numberBook.innerText = books.length;
};